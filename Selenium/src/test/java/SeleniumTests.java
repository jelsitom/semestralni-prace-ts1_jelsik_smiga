import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;


public class SeleniumTests {

    WebDriver driver;

    @BeforeEach
    public void setUp(){
        driver = new ChromeDriver();
    }

    @Test
    public void uvodTextTest() {
        driver.get("https://wa.toad.cz/~smigapav/Semestralka/home/home.php");
        String actualText = "Finanční zdraví je klíčovým prvkem pro dosažení osobního a profesního úspěchu. Může ovlivnit vše, od Vaši schopnosti splácet úvěry, až po Vaši schopnost žít pohodlný život a plnit si své sny. Ačkoli může být těžké se o své finanční zdraví postarat, je to něco, co můžeme všichni udělat." +
                "Prvním krokem k lepšímu finančnímu zdraví je získat přehled o svých finančních prostředcích. To zahrnuje zjištění, kolik peněz máte přístupných, kolik dluhů máte a kolik peněz utrácíte. Toto Vám umožní vytvořit rozpočet, který Vám pomůže lépe plánovat své výdaje a udržet si finanční stabilitu." +
                "Dalším krokem je naučit se šetřit a investovat. Šetření Vám umožní připravit se na neočekávané výdaje a investování Vám může pomoci zhodnotit Vaše peníze a zajistit finanční budoucnost. Je důležité vybrat si správné finanční produkty pro Vaše potřeby a dobře se informovat o možnostech, abyste mohli učinit správná rozhodnutí." +
                "Pokud se chcete postarat o své finanční zdraví, je důležité věnovat tomu pozornost a učit se jak na to. Naše webová stránka ohodnotí Vaše finanční zdraví a poskytne Vám tipy, jak zlepšit své finanční zdraví a dosáhnout finanční stability.";

        StringBuilder sb = new StringBuilder();
        sb.append(driver.findElement(By.id("home_text1")).getText());
        sb.append(driver.findElement(By.id("home_text2")).getText());
        sb.append(driver.findElement(By.id("home_text3")).getText());
        sb.append(driver.findElement(By.id("home_text4")).getText());

        Assertions.assertEquals(actualText, sb.toString());
    }
}
