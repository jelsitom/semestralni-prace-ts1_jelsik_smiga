<nav class="blur_background">
<div class="logo">
    <img src="../img/png_white_logo_smaller.png" alt="Logo">
</div>
<div class="tabs">
    <a class="tab" href="
    <?php
    /**
     * If the user isn't logged in, change the link to login page
     */
    if (isset($_SESSION["user"])){
        echo ("../quiz/quiz.php");
    }
    else{
        echo ("../login/login.php");
    }
    ?>">
        <div>Kvíz</div>
    </a>
    <a class="tab" href="../home/home.php">
        <div>Úvod</div>
    </a>
    <a class="tab" href="<?php
    /**
     * If the user isn't logged in, change the link to login page
     */
    if (isset($_SESSION["user"])){
        echo ("../results/results.php");
    }
    else{
        echo ("../login/login.php");
    }
    ?>">
        <div>Výsledky kvízu</div>
    </a>
    <?php
    /**
     * If the user is logged in, change the link to logout page
     */
    if (isset($_SESSION["user"])){
        echo ('<a class="tab" href="../php/logoutUser.php"><div>Odhlásit se</div></a>');
    }
    else{
        echo ('<a class="tab" href="../login/login.php"><div>Přihlásit se</div></a>');
    }
    ?>
</div>
<div class="appearance_modes">
    <a href="?mode=colorless">
        <div class="dark_mode"></div>
    </a>
    <a href="?mode=normal">
        <div class="normal_mode"></div>
    </a>
</div>
</nav>