<?php
/**
 * @brief Resumes the user's session and displays the latest quiz data
 *
 * This script starts a new session or resumes an existing one, includes a file to set a cookie for the user's chosen appearance, redirects the user to the login page if they are not logged in, includes a library file, and retrieves the latest quiz data from the database. If no data is found, the user is redirected to the quiz page.
 */

/**
 * @brief Start or resume a session
 *
 * This statement starts a new session or resumes an existing one.
 */
if (session_id() === '') {
    session_start();
}

/**
 * @brief Include file to set cookie for appearance
 *
 * This include statement includes the file "setCookieForAppearance.php" which sets a cookie for the user's chosen appearance.
 */
include ("../php/setCookieForAppearance.php");

/**
 * @brief Redirect user to login page if not logged in
 *
 * This statement checks if the user is not logged in by checking the SESSION variable "user". If the user is not logged in, they are redirected to the login page.
 */
if (!isset($_SESSION["user"])){
    header("Location: ../login/login.php");
}

/**
 * @brief Include library file
 *
 * This include statement includes the file "library.php" which contains various functions that may be used throughout the script.
 */
include ("../php/library.php");

/**
 * @brief Retrieve the latest quiz data
 *
 * This variable stores the latest quiz data retrieved from the database by calling the getNewestData() function.
 */
$data = getNewestData();

/**
 * @brief Redirect user to quiz page if no data is found
 *
 * This statement checks if the @param array $data variable is null. If it is, the user is redirected to the quiz page.
 */
if ($data === null){
    header("Location: ../quiz/quiz.php");
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php
    include("../php/appearanceGlobal.php");
    include("../php/appearanceResults.php")
    ?>
    <link rel="icon" href="../img/tab_icon.png">
    <title>Financial (W/H)ealth</title>
</head>
<body>
<?php
include("../header.php");
$current_page = (isset($_GET['page']) && in_array($_GET['page'], ["1", "2", "3"])) ? (int)$_GET['page'] : 1;
?>
<main>
    <h2>Výsledky</h2>
    <div class="results">
    <?php
    /**
     * This part of code depends on the data from quiz and the page, the user has selected.
     * Depending on this, there will be different text shown.
     */
    switch ($current_page){
        case "1":
            echo ('
            <p class="text blur_background border">
                Přehled
                <br/>
                <br/>
            ');
                switch ($data["q1"]){
                    case "1":
                        echo ("Pokud nejste chodící počítač, doporučujeme Vám si sjednat a pak také pravidelně používat internetové nebo mobilní bankovnictví. Jakmile uvidíte, kudy, kam a v jakém množství vaše peníze proudí, můžete hodně ušetřit.");
                        break;
                    case "2":
                        echo ("Zasloužíte si pochvalu, že Vám nejsou vaše peníze putna. Přesto Vám doporučujeme sledovat kromě výplaty i to, co vám z účtu odchází a za co přesně utrácíte. S internetovým nebo mobilním bankovnictvím budete mít kdykoliv okamžitý přehled a spíš tak něco ušetříte.");
                        break;
                    case "3":
                        echo ("Výborně! Svým penězům věnujete pozornost, víte, na čem jste, a nic Vás nezaskočí. Pokud máte jen internetové bankovnictví, doporučujeme Vám stáhnout si do telefonu i mobilní verzi, se kterou získáte k penězům bezpečný přístup, a to kdekoliv a nonstop");
                        break;
                }
            echo ('
            </p>
            <p class="text blur_background border">
                Placení účtů
                <br/>
                <br/>
            ');
                switch ($data["q2"]){
                    case "1":
                        echo ("Uf! Nechávat všechno na poslední chvíli nebo ještě hůř platit s křížkem po funuse Vás může přijít pěkně draho. Ne každý Vám promine opožděnou platbu bez penále. V internetovém nebo mobilním bankovnictví si zadáte trvalé příkazy nebo souhlas s inkasem snadno a rychle a budete mít klid.");
                        break;
                    case "2":
                        echo ("Smekáme! Jestli s takovým systémem stíháte platit své účty včas, všechna čest. Ale víte, jak se to říká s tím džbánem a utrženým uchem. Pokud chcete ušetřit čas a sobě starosti, zadejte si během chvilky v internetovém nebo mobilním bankovnictví trvalé příkazy nebo inkasní platby.");
                        break;
                    case "3":
                        echo ("Zasloužíte pochvalu před nastoupeným mužstvem. Je vidět, že se ke svým závazkům stavíte zodpovědně, což je vaše velké plus i v očích současných nebo budoucích věřitelů. Jen tak dál.");
                        break;
                }
            echo ('
            </p>
            <p class="text blur_background border">
                Naspořeno
                <br/>
                <br/>
            ');
                switch ($data["q3"]){
                    case "1":
                        echo ("Mít naspořeno dost na nové auto je pro mnoho lidí pouhý sen. Pokud ale nemáte peněžní polštář ani pro měsíc běžného provozu domácnosti, může i jedna nečekaná událost pěkně zabolet.");
                        break;
                    case "3":
                    case "2":
                        echo ("Mít naspořeno aspoň něco málo je pořád lepší než nic. Takhle aspoň nemusíte propadat panice, když se doma něco rozbije. Zkuste si ale odkládat stranou alespoň o pár stovek více. Uvidíte, že Vám to přinese do života větší pocit jistoty a klidu.");
                        break;
                    case "4":
                        echo ("Paráda. Teď už jde jen o to, uložit peníze někam, kde je budete mít v případě potřeby po ruce a zároveň neztratí na hodnotě.");
                        break;
                }
            echo ('
            </p>
            <p class="text blur_background border">
                Finanční svoboda
                <br/>
                <br/>
            ');
                switch ($data["q4"]){
                    case "1":
                        echo ("Chápeme, že ne každému se podaří urvat nějaké peníze jen pro sebe. Zkuste si ale něco málo odkládat stranou, ať si pak můžete odfrknout na dovolené nebo si koupit něco většího.");
                        break;
                    case "2":
                        echo ("Každá uspořená koruna se počítá. Zkuste se podívat na své výdaje, jestli zvládnete na něčem ještě ušetřit a získat tím další peníze navíc.");
                        break;
                    case "3":
                        echo ("Je super, když ušetříte dost, aby Vám peníze přinášely také radost a svobodu. Myslete na to, že vhodným produktem pro vaše přání a cíle jsou investice a stavební spoření, které chrání peníze před inflací");
                        break;
                }
            echo ('
            </p>
            ');
            break;
        case "2":
            echo ('
            <p class="text blur_background border">
                Výdaje na život
                <br/>
                <br/>
            ');
                if ($data["q5"] <= 8000){
                    echo ("Výborně. Je vidět, že neutrácíte za blbinky a kupujete jen to, co potřebujete. Ostatní si z Vás mohou vzít příklad.");
                }
                elseif ($data["q5"] >= 20000){
                    echo ("Buď jste samaritán, který dává všechno chudým, nebo rychle přibrzděte. Utrácením za věci, které nepotřebujete, se zbytečně připravujete o rezervu na horší časy nebo investici do budoucna.");
                }
                else{
                    echo ("Zkuste si napsat seznam všeho, co si každý měsíc kupujete. Určitě objevíte pár položek, které můžete propříště škrtnout a ušetřené peníze vložit někam, kde to dává větší smysl.");
                }
            echo ('
            </p>
            <p class="text blur_background border">
                Způsob placení
                <br/>
                <br/>
            ');
                switch ($data["q6"]){
                    case "1":
                        echo ("Ajaj! Pravděpodobně jste dítě štěstěny, ale statisticky vzato dost riskujete, že Vás o peníze v kapse někdo připraví nebo že Vám vypadnou. Navíc s tímhle způsobem placení nemáte vůbec kontrolu nad tím, kolik a za co utrácíte.");
                        break;
                    case "2":
                        echo ("Asi máte dobrý důvod, proč platíte hotově. Pokud si k tomu vedete domácí účetnictví a máte přehled ve svých výdajích, gratulujeme. Určitě ale v dnešní době doporučujeme u sebe platební kartu aspoň mít. Její používání je pohodlné a hlavně bezpečné. Kromě toho všechny platby kartou uvidíte přehledně v mobilním bankovnictví.");
                        break;
                    case "3":
                        echo ("Jdete s dobou, výborně. Dnes už se dá kartou zaplatit téměř všude. Nemusíte nikde shánět bankomat a o svých platbách máte dokonalý přehled díky mobilnímu bankovnictví. Pokud s sebou nechcete nosit peněženku s kartou, vyzkoušejte placení mobilem nebo hodinkami.");
                        break;
                }
            echo ('
            </p>
            <p class="text blur_background border">
                Bydlení
                <br/>
                <br/>
            ');
                switch ($data["q7"]){
                    case "1":
                    case "2":
                        echo ("Hledejte úspory, kde se dá. Zkuste třeba srovnávače energií. Nebo si ověřte, jestli nemáte nárok na příspěvek na bydlení.");
                        break;
                    case "3":
                    case "4":
                        echo ("Bydlení zvládáte hravě a nepředstavuje pro Vás významný risk.");
                        break;
                }
            echo ('
            </p>
            <p class="text blur_background border">
                Splátkové zatížení
                <br/>
                <br/>
            ');
                switch ($data["q9_amount"]){
                    case "1":
                        if ($data["q8_amount"] <= 3000){
                            echo ("Vypadá to, že půjčky máte pod kontrolou. Jen tak dál.");
                        }
                        elseif ($data["q8_amount"] >= 8000){
                            echo ("Půjčky Vás asi dost dusí, že? Tak si hlavně neberte novou a nechte si poradit od Vašeho bankéřem, jak to udělat, abyste mohli zase svobodně dýchat. Více půjček je možné sloučit do jedné a z nevýhodné půjčky udělat takovou, kterou zvládnete.");
                        }
                        else{
                            echo ("Splácení zatím máte v malíku, ale bacha, ať Vám nepřeroste přes hlavu. Kdyby něco takového hrozilo, můžete si udělat z více půjček jednu, kterou zvládnete.");
                        }
                        break;
                    case "2":
                        if ($data["q8_amount"] <= 5000){
                            echo ("Vypadá to, že půjčky máte pod kontrolou. Jen tak dál.");
                        }
                        elseif ($data["q8_amount"] >= 10000){
                            echo ("Půjčky Vás asi dost dusí, že? Tak si hlavně neberte novou a nechte si poradit od Vašeho bankéřem, jak to udělat, abyste mohli zase svobodně dýchat. Více půjček je možné sloučit do jedné a z nevýhodné půjčky udělat takovou, kterou zvládnete.");
                        }
                        else{
                            echo ("Splácení zatím máte v malíku, ale bacha, ať Vám nepřeroste přes hlavu. Kdyby něco takového hrozilo, můžete si udělat z více půjček jednu, kterou zvládnete.");
                        }
                        break;
                    case "3":
                        if ($data["q8_amount"] <= 8000){
                            echo ("Vypadá to, že půjčky máte pod kontrolou. Jen tak dál.");
                        }
                        elseif ($data["q8_amount"] >= 15000){
                            echo ("Půjčky Vás asi dost dusí, že? Tak si hlavně neberte novou a nechte si poradit od Vašeho bankéřem, jak to udělat, abyste mohli zase svobodně dýchat. Více půjček je možné sloučit do jedné a z nevýhodné půjčky udělat takovou, kterou zvládnete.");
                        }
                        else{
                            echo ("Splácení zatím máte v malíku, ale bacha, ať Vám nepřeroste přes hlavu. Kdyby něco takového hrozilo, můžete si udělat z více půjček jednu, kterou zvládnete.");
                        }
                        break;
                    case "4":
                        if ($data["q8_amount"] <= 10000){
                            echo ("Vypadá to, že půjčky máte pod kontrolou. Jen tak dál.");
                        }
                        elseif ($data["q8_amount"] >= 20000){
                            echo ("Půjčky Vás asi dost dusí, že? Tak si hlavně neberte novou a nechte si poradit od Vašeho bankéřem, jak to udělat, abyste mohli zase svobodně dýchat. Více půjček je možné sloučit do jedné a z nevýhodné půjčky udělat takovou, kterou zvládnete.");
                        }
                        else{
                            echo ("Splácení zatím máte v malíku, ale bacha, ať Vám nepřeroste přes hlavu. Kdyby něco takového hrozilo, můžete si udělat z více půjček jednu, kterou zvládnete.");
                        }
                        break;
                    case "5":
                        if ($data["q8_amount"] <= 15000){
                            echo ("Vypadá to, že půjčky máte pod kontrolou. Jen tak dál.");
                        }
                        elseif ($data["q8_amount"] >= 30000){
                            echo ("Půjčky Vás asi dost dusí, že? Tak si hlavně neberte novou a nechte si poradit od Vašeho bankéřem, jak to udělat, abyste mohli zase svobodně dýchat. Více půjček je možné sloučit do jedné a z nevýhodné půjčky udělat takovou, kterou zvládnete.");
                        }
                        else{
                            echo ("Splácení zatím máte v malíku, ale bacha, ať Vám nepřeroste přes hlavu. Kdyby něco takového hrozilo, můžete si udělat z více půjček jednu, kterou zvládnete.");
                        }
                        break;
                    case "6":
                        if ($data["q8_amount"] <= 30000){
                            echo ("Vypadá to, že půjčky máte pod kontrolou. Jen tak dál.");
                        }
                        elseif ($data["q8_amount"] >= 60000){
                            echo ("Půjčky Vás asi dost dusí, že? Tak si hlavně neberte novou a nechte si poradit od Vašeho bankéřem, jak to udělat, abyste mohli zase svobodně dýchat. Více půjček je možné sloučit do jedné a z nevýhodné půjčky udělat takovou, kterou zvládnete.");
                        }
                        else{
                            echo ("Splácení zatím máte v malíku, ale bacha, ať Vám nepřeroste přes hlavu. Kdyby něco takového hrozilo, můžete si udělat z více půjček jednu, kterou zvládnete.");
                        }
                        break;
                }
            echo ('
            </p>
            ');
            break;
        case "3":
            echo ('
            <p class="text blur_background border">
                Nečekané výdaje
                <br/>
                <br/>
            ');
                switch ($data["q10"]){
                    case "1":
                        echo ("Rychlé malé půjčky Vás mohou přijít pěkně draho. Zkuste příště řešit situaci trochu jinak. Začněte tím, že se zeptáte u své banky.");
                        break;
                    case "3":
                    case "2":
                        echo ("Zrušit spořicí produkt předčasně je dost nevýhodné a na unáhleném prodeji investic můžete hodně prodělat. Bylo by fajn začít si odkládat něco stranou, ať se podobným řešením vyhnete. Že nic neušetříte? Malý trik – dejte si něco stranou hned po výplatě.");
                        break;
                    case "4":
                        echo ("Výborně. Máte kam sáhnout a to je to nejlepší možné řešení. Jen pozor, ať toho nemáte na spořicím účtu zase moc. Rádi Vám povíme o dalších možnostech zhodnocení, jako je třeba pravidelné investování.");
                        break;
                }
            echo ('
            </p>
            <p class="text blur_background border">
                Pojištění
                <br/>
                <br/>
            ');
                if ($data["q11"] == [6]){
                    echo ("Našlapujte velmi zlehka, jste totiž na tenkém ledě. Úraz Vás může připravit o příjem, zloděj o televizi a požár o zbytek. Pak bude pozdě. Přitom když si platby pojistného rozdělíte po měsících, je to nízká cena výměnou za klidný spánek. Poraďte se s bankéřem, pomůžem Vám s volbou vhodného pojištění.");
                }
                elseif (count($data["q11"]) == (count($data["q11_options"]) + 2)){
                    echo ("Jak se říká, štěstí přeje připraveným – a to je i Váš případ. Jen se občas nezapomeňte na své smlouvy mrknout s někým, kdo tomu rozumí. I pojistky totiž stárnou a za čas už nemusejí stačit Vašemu životnímu tempu nebo rostoucímu majetku.");
                }
                else{
                    echo ("Něco pojistit a něco ne je jako vzít si jen levou botu. Jeden krok máte jistý, druhý nejistý. Životní pojištění se hodí, když živíte rodinu, úrazové pojištění by měl mít každý, kdo chodí po světě, a pojištění majetku ten, kdo něco má. Poraďte se s bankéřem a dotáhněte svou ochranu do konce.");
                }
            echo ('
            </p>
            <p class="text blur_background border">
                Budoucnost
                <br/>
                <br/>
            ');
                switch ($data["q12"]){
                    case "1":
                        echo ("Budoucnost asi zatím moc neřešíte, protože je to budoucnost. Jednoho dne ale přijde a pak pozdě bycha honit. Začněte si spořit co nejdříve, stačí pár stovek měsíčně a bude Vám přispívat i stát. Nebo zkuste investování, ať pak z toho života taky něco máte.");
                        break;
                    case "2":
                    case "3":
                        echo ("Je super, že myslíte i na penzi. Otázkou je, zda to bude stačit na všechny výdaje, koníčky nebo cestování. Víte, že 1 000 Kč měsíčně na penzijním spoření Vám už zajistí maximální státní příspěvek? Navíc Vám může přispívat i zaměstnavatel.");
                        break;
                    case "4":
                        echo ("Bezva, bezva. Svou budoucnost nenecháváte v rukou druhých a zatím to vypadá, že budete mít dost na všechny výdaje, koníčky, vnoučata i cestování. Pokud si přispíváte 3 000 Kč na penzijní spoření, máte plnou státní podporu, maximální daňové zvýhodnění, a ještě Vám může přispívat zaměstnavatel. Zeptejte se ho na to.");
                        break;
                }
            echo ('
            </p>
            ');
            break;
    }
    echo ('</div>');
    /**
     * @brief Generates pagination links
     *
     * This script generates a set of pagination links based on the total number of pages and the current page. The links are enclosed in a div with the class "pages blur_background border". The current page is displayed as plain text and the other pages are displayed as links that redirect to the specified page when clicked.
     *
     * @param int $current_page The current page number
     * @param int $total_pages The total number of pages
     */

    echo ('<div class="pages blur_background border">');
    $total_pages = 3;

    /**
     * @brief Generate pagination links
     *
     * This for loop generates the pagination links. The loop iterates from 1 to the total number of pages. On each iteration, it checks if the current page number is equal to the loop variable. If it is, the page number is displayed as plain text. If it is not, the page number is displayed as a link that redirects to the specified page when clicked.
     */
    for ($i = 1; $i <= $total_pages; $i++) {
        if ($i == $current_page) {
            echo $i . ' ';
        } else {
            echo "<a href='?page=".$i."'>".$i."</a> ";
        }
    }
    echo ('</div>');

    ?>
</main>
</body>
</html>