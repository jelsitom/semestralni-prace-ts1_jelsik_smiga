<?php
/**
 * Start a new session or resume an existing one.
 */
if (session_id() === '') {
   session_start();
}

include ("../php/setCookieForAppearance.php");

include ("../php/library.php");
/**
 * Check if the values from the form are set, and if they are, check their validity.
 * If everything checks out, add the user to the users.json file and redirect user to the home page.
 * Otherwise, fill the input fields with the values and let the user fix them.
 */
if (isset($_POST["username"]) and
    isset($_POST["password1"]) and
    isset($_POST["password2"])){
    if (isset($_POST["name"])){
        $name = htmlspecialchars($_POST["name"]);
    }
    else{
        $name = "";
    }
    if (isset($_POST["surname"])){
        $surname = htmlspecialchars($_POST["surname"]);
    }
    else{
        $surname = "";
    }
    $username = htmlspecialchars($_POST["username"]);
    $password1 = $_POST["password1"];
    $password2 = $_POST["password2"];
    $id = uniqid();

    if (checkName($name) and checkName($surname) and $password1 == $password2 and
        checkPasswordStrength($password1) == "strong" and checkUsername($username)){
        addUser($name, $surname, $username, $password1, $id);
        $_SESSION["user"] = $username;
        header("Location: ../home/home.php");
    }
}
else{
    if (isset($_POST["username"])){
        $username = htmlspecialchars($_POST["username"]);
    }
    if (isset($_POST["name"])){
        $name = htmlspecialchars($_POST["name"]);
    }
    if (isset($_POST["surname"])){
        $surname = htmlspecialchars($_POST["surname"]);
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php
    include("../php/appearanceGlobal.php");
    ?>
    <link rel="stylesheet" href="signup.css">
    <link rel="icon" href="../img/tab_icon.png">
    <title>Financial (W/H)ealth</title>
</head>
<body>
<?php
include("../header.php");
?>
    <main>
        <div class="white_logo">
            <img src="../img/png_white_logo.png" alt="Financial (W/H)ealth Logo">
        </div>
        <div class="signup blur_background border">
            <div class="headline pad align_center">
                <h2>Registrace</h2>
            </div>
            <form method="post" id="signup_form">
                <div class="align_in_row pad">
                    <div class="name">
                        <h3><label for="name">Jméno: </label></h3>
                    </div>
                    <div class="form">
                        <input class="input" type="text" name="name" id="name" value="<?php if (isset($name)){echo ($name);} ?>" placeholder="Jméno" pattern="[A-Za-zÁČĎÉĚÍŇÓŘŠŤÚŮÝŽáčďéěíňóřšťúůýž]*">
                    </div>
                </div>
                <div class="align_in_row pad">
                    <div class="name">
                        <h3><label for="surname">Příjmení: </label></h3>
                    </div>
                    <div class="form">
                        <input class="input" type="text" name="surname" id="surname" value="<?php if (isset($surname)){echo ($surname);} ?>" placeholder="Příjmení" pattern="[A-Za-zÁČĎÉĚÍŇÓŘŠŤÚŮÝŽáčďéěíňóřšťúůýž]*">
                    </div>
                </div>
                <div class="align_in_row pad">
                    <div class="name">
                        <h3><label for="username">*Uživatelské jméno: </label></h3>
                    </div>
                    <div class="form">
                        <input class="input" type="text" name="username" id="username" value="<?php if (isset($username)){echo ($username);} ?>" placeholder="Uživatelské jméno" required>
                    </div>
                </div>
                <div class="align_in_row pad">
                    <div class="name">
                        <h3><label for="password1">*Heslo: </label></h3>
                    </div>
                    <div class="form">
                        <input class="input" type="password" name="password1" id="password1" placeholder="Heslo" pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[^a-zA-Z0-9]).{8,}$" required>
                    </div>
                </div>
                <div class="align_in_row pad">
                    <div class="name">
                        <h3><label for="password2">*Heslo: </label></h3>
                    </div>
                    <div class="form">
                        <input class="input" type="password" name="password2" id="password2" placeholder="Heslo" pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[,.-?:_/§!#&@%^*+]).{8,}$" required>
                    </div>
                </div>
                <div class="message">
                    <h4 id="message1">Heslo musí obsahovat minimálně 8 znaků, jedno malá písmeno, <br>jedno velké písmeno, jednu číslici a jeden speciální znak.<br></h4>
                    <h4 id="message2">Hesla se neshodují.<br></h4>
                    <h4 id="message3">Uživatelské jméno již někdo používá.<br></h4>
                    <h4 id="message5">Pole označena * jsou povinná.<br></h4>
                </div>
                <div class="submit align_in_row pad">
                    <button>Registruj se</button>
                </div>
            </form>
        </div>
    </main>
    <script src="../scripts/signupFormCheck.js"></script>
</body>
</html>