<?php
/**
 * Start a new session or resume an existing one.
 */
if (session_id() === '') {
   session_start();
}
include ("../php/setCookieForAppearance.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php
    include("../php/appearanceGlobal.php");
    include("../php/appearanceHome.php")
    ?>
    <link rel="icon" href="../img/tab_icon.png">
    <title>Financial (W/H)ealth</title>
</head>
<body>
<?php
include("../header.php");
?>
<div class="headline">
    <h2>Úvod</h2>
</div>
<div class="content">
    <p class="text blur_background border">
        Finanční zdraví je klíčovým prvkem pro dosažení osobního a profesního úspěchu.
        Může ovlivnit vše, od Vaši schopnosti splácet úvěry, až po Vaši schopnost žít pohodlný život a plnit si své sny.
        Ačkoli může být těžké se o své finanční zdraví postarat, je to něco, co můžeme všichni udělat.
    </p>
    <p class="text blur_background border">
        Prvním krokem k lepšímu finančnímu zdraví je získat přehled o svých finančních prostředcích.
        To zahrnuje zjištění, kolik peněz máte přístupných, kolik dluhů máte a kolik peněz utrácíte.
        Toto Vám umožní vytvořit rozpočet, který Vám pomůže lépe plánovat své výdaje a udržet si finanční stabilitu.
        </p>
    <p class="text blur_background border">
        Dalším krokem je naučit se šetřit a investovat.
        Šetření Vám umožní připravit se na neočekávané výdaje a investování Vám může pomoci zhodnotit Vaše peníze a zajistit finanční budoucnost.
        Je důležité vybrat si správné finanční produkty pro Vaše potřeby a dobře se informovat o možnostech, abyste mohli učinit správná rozhodnutí.
    </p>
    <p class="text blur_background border">
        Pokud se chcete postarat o své finanční zdraví, je důležité věnovat tomu pozornost a učit se jak na to.
        Naše webová stránka ohodnotí Vaše finanční zdraví a poskytne Vám tipy, jak zlepšit své finanční zdraví a dosáhnout finanční stability.
    </p>
</div>
</body>
</html>