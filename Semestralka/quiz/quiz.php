<?php

/**
 * Start a new session or resume an existing one.
 */
if (session_id() === '') {
    session_start();
}

/**
 * @file
 *
 * This script includes a file to set a cookie for the user's chosen appearance, redirects
 * the user to the login page if they are not logged in and includes a library file.
 */

/**
 * @brief Include file to set cookie for appearance
 *
 * This include statement includes the file "setCookieForAppearance.php" which sets a cookie for the user's chosen appearance.
 */
include ("../php/setCookieForAppearance.php");

/**
 * @brief Redirect user to login page if not logged in
 *
 * This statement checks if the user is not logged in by checking the SESSION variable "user". If the user is not logged in, they are redirected to the login page.
 */
if (!isset($_SESSION["user"])){
    header("Location: ../login/login.php");
}

/**
 * @brief Include library file
 *
 * This include statement includes the file "library.php" which contains various functions that may be used throughout the script.
 */
include ("../php/library.php");

/**
 * @brief Check if options for question 11 were submitted
 *
 * This statement checks if the $_POST variable "q11_options" has been set. If it has, it proceeds to check if the variable is not empty. If it is not empty, it assigns the variable to $q11_options. If it is empty or the $_POST variable was not set, it assigns an empty array to $q11_options.
 */
if (isset($_POST["q11_options"])){
    if (!empty($_POST["q11_options"])){
        $q11_options = $_POST["q11_options"];
    }
    else{
        $q11_options = [];
    }
}
else{
    $q11_options = [];
}

/**
 * @brief Check if question 11 was submitted
 *
 * This statement checks if the $_POST variable "q11" has been set. If it has, it proceeds to check if the variable is not empty. If it is not empty, it assigns the variable to $q11. If it is empty or the $_POST variable was not set, it assigns the value ["6"] to $q11.
 */
if (isset($_POST["q11"])){
    if (!empty($_POST["q11"])){
        $q11 = $_POST["q11"];
    }
    else{
        $q11 = ["6"];
    }
}
else{
    $q11 = null;
}

/**
 * @brief Validate quiz form data
 *
 * This statement checks if the following variables are set in the $_POST array and if their values are valid. If all conditions are met, it redirects the user to the results page, adds the data to the database and exits the script.
 */
if (isset($_POST["q1"]) && in_array($_POST["q1"], ["1","2","3"]) &&
    isset($_POST["q2"]) && in_array($_POST["q2"], ["1","2","3"]) &&
    isset($_POST["q3"]) && in_array($_POST["q3"], ["1","2","3","4"]) &&
    isset($_POST["q4"]) && in_array($_POST["q4"], ["1","2","3"]) &&
    isset($_POST["q5"]) && $_POST["q5"] >= "1000" && $_POST["q5"] <= "30000" &&
    isset($_POST["q6"]) && in_array($_POST["q6"], ["1","2","3"]) &&
    isset($_POST["q7"]) && in_array($_POST["q7"], ["1","2","3","4"]) &&
    isset($_POST["q8_amount"]) && $_POST["q8_amount"] >= "0" && $_POST["q8_amount"] <= "30000" &&
    isset($_POST["q9_amount"]) && $_POST["q9_amount"] >= "1" && $_POST["q9_amount"] <= "6" &&
    isset($_POST["q10"]) && in_array($_POST["q10"], ["1","2","3","4"]) &&
    isset($_POST["q12"]) && in_array($_POST["q12"], ["1","2","3","4"]) &&
    checkQuizFormData($q11, $q11_options)){
        // redirect to results page if the data is valid
        header("Location: ../results/results.php");
        // add data to the database
        addData($_POST["q1"], $_POST["q2"], $_POST["q3"], $_POST["q4"], $_POST["q5"], $_POST["q6"], $_POST["q7"], $_POST["q8_amount"], $_POST["q9_amount"], $_POST["q10"], $q11_options, $q11, $_POST["q12"]);
        exit;
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php
    include("../php/appearanceGlobal.php");
    include("../php/appearanceQuiz.php")
    ?>
    <link rel="icon" href="../img/tab_icon.png">
    <link rel="script" href="../scripts/rangeInputValue.js">
    <link rel="script" href="../scripts/disableCheckboxes.js">
    <link rel="script" href="../scripts/disabeledJavaScript.js">
    <link rel="script" href="../scripts/quizFormCheck.js">
    <title>Financial (W/H)ealth</title>
</head>
<body>
<?php
include("../header.php");
?>
<div class="headline">
    <h2>Odpovězte prosím na všechny otázky.</h2>
</div>
<form method="post" class="quiz_form">
    <div class="quiz_3q blur_background border">
        <h2 class="question">Máte o svých penězích
            vždycky přehled?</h2>
        <label class="answer_option_3q border" >
            <input type="radio" name="q1" value="1" <?php if (isset($_POST["q1"]) && $_POST["q1"] == "1"){echo "checked";} elseif (!isset($_POST["q1"])){echo "checked";} ?>><a class="answer_text">Moc ne, spíš tak nějak tuším, jak jsem na tom.</a>
        </label>
        <label class="answer_option_3q border" >
            <input type="radio" name="q1" value="2" <?php if (isset($_POST["q1"]) && $_POST["q1"] == "2"){echo "checked";} ?>><a class="answer_text">Jednou za čas se podívám, jestli mi přišly peníze.</a>
        </label>
        <label class="answer_option_3q border" >
            <input type="radio" name="q1" value="3" <?php if (isset($_POST["q1"]) && $_POST["q1"] == "3"){echo "checked";} ?>><a class="answer_text">Několikrát do měsíce nahlédnu do internetového nebo mobilního bankovnictví.</a>
        </label>
    </div>
    <div class="quiz_3q blur_background border">
        <h2 class="question">Jak platíte své pravidelné platby?</h2>
        <label class="answer_option_3q border" >
            <input type="radio" name="q2" value="1" <?php if (isset($_POST["q2"]) && $_POST["q2"] == "1"){echo "checked";} elseif (!isset($_POST["q1"])){echo "checked";} ?>><a class="answer_text">Platím, až když se mi někdo ozve.</a>
        </label>
        <label class="answer_option_3q border" >
            <input type="radio" name="q2" value="2" <?php if (isset($_POST["q2"]) && $_POST["q2"] == "2"){echo "checked";} ?>><a class="answer_text">Dávám si účty na hromadu a jednou za čas je zaplatím.</a>
        </label>
        <label class="answer_option_3q border" >
            <input type="radio" name="q2" value="3" <?php if (isset($_POST["q2"]) && $_POST["q2"] == "3"){echo "checked";} ?>><a class="answer_text">Mám nastavené trvalé příkazy nebo inkasa. Ostatní platím hned.</a>
        </label>
    </div>
    <div class="quiz_4q blur_background border">
        <h2 class="question">Dáváte si nějaké peníze stranou?
            Jak dlouho byste vydrželi bez příjmu?</h2>
        <label class="answer_option_4q border" >
            <input type="radio" name="q3" value="1" <?php if (isset($_POST["q3"]) && $_POST["q3"] == "1"){echo "checked";} elseif (!isset($_POST["q1"])){echo "checked";} ?>><a class="answer_text">Sotva pár dnů.</a>
        </label>
        <label class="answer_option_4q border" >
            <input type="radio" name="q3" value="2" <?php if (isset($_POST["q3"]) && $_POST["q3"] == "2"){echo "checked";} ?>><a class="answer_text">Asi tak měsíc.</a>
        </label>
        <label class="answer_option_4q border" >
            <input type="radio" name="q3" value="3" <?php if (isset($_POST["q3"]) && $_POST["q3"] == "3"){echo "checked";} ?>><a class="answer_text">Mám dost na čtvrt roku.</a>
        </label>
        <label class="answer_option_4q border" >
            <input type="radio" name="q3" value="4" <?php if (isset($_POST["q3"]) && $_POST["q3"] == "4"){echo "checked";} ?>><a class="answer_text">Zvládnu i půl roku bez příjmu.</a>
        </label>
    </div>
    <div class="quiz_3q blur_background border">
        <h2 class="question">Dělají vám peníze také radost,
            nebo jen platíte účty?</h2>
        <label class="answer_option_3q border" >
            <input type="radio" name="q4" value="1" <?php if (isset($_POST["q4"]) && $_POST["q4"] == "1"){echo "checked";} elseif (!isset($_POST["q1"])){echo "checked";} ?>><a class="answer_text">Žiju ze dne na den a z ruky do pusy.</a>
        </label>
        <label class="answer_option_3q border" >
            <input type="radio" name="q4" value="2" <?php if (isset($_POST["q4"]) && $_POST["q4"] == "2"){echo "checked";} ?>><a class="answer_text">Ale jo, sem tam si něco dopřeju.</a>
        </label>
        <label class="answer_option_3q border" >
            <input type="radio" name="q4" value="3" <?php if (isset($_POST["q4"]) && $_POST["q4"] == "3"){echo "checked";} ?>><a class="answer_text">S pomocí pravidelných investic a spoření si můžu dopřát, co chci.</a>
        </label>
    </div>
    <div class="quiz_3q blur_background border">
        <h2 class="question">
            <label for="q5_range_input">Kolik průměrně utratíte za měsíc?</label>
        </h2>
        <input type="range" min="1000" max="30000" step="1000" value="<?php if (isset($_POST["q5"])){echo $_POST["q5"];} else{echo "1000";} ?>" name="q5" class="range_input" id="q5_range_input">
        <output class="number_output" id="q5_output" name="result" for="q5_range_input"><?php if (isset($_POST["q5"])){echo $_POST["q5"];} else{echo "1000";} ?> Kč</output>
    </div>
    <div class="quiz_3q blur_background border">
        <h2 class="question">Jak nejčastěji platíte za své nákupy?</h2>
        <label class="answer_option_3q border" >
            <input type="radio" name="q6" value="1" <?php if (isset($_POST["q6"]) && $_POST["q6"] == "1"){echo "checked";} elseif (!isset($_POST["q1"])){echo "checked";} ?>><a class="answer_text">Platím tím, co zrovna najdu v kapse.</a>
        </label>
        <label class="answer_option_3q border" >
            <input type="radio" name="q6" value="2" <?php if (isset($_POST["q6"]) && $_POST["q6"] == "2"){echo "checked";} ?>><a class="answer_text">Nejvíce mi vyhovuje platit hotově.</a>
        </label>
        <label class="answer_option_3q border" >
            <input type="radio" name="q6" value="3" <?php if (isset($_POST["q6"]) && $_POST["q6"] == "3"){echo "checked";} ?>><a class="answer_text">Tam, kde to jde, platím kartou nebo mobilem.</a>
        </label>
    </div>
    <div class="quiz_3q blur_background border">
        <h2 class="question">Jak to máte s náklady na bydlení?</h2>
        <div class="quiz_answeres_fix_4q">
            <label class="answer_option_4q border" >
                <input type="radio" name="q7" value="1" <?php if (isset($_POST["q7"]) && $_POST["q7"] == "1"){echo "checked";} elseif (!isset($_POST["q1"])){echo "checked";} ?>><a class="answer_text">Je to problém, musí mi pomáhat rodina nebo přátelé.</a>
            </label>
            <label class="answer_option_4q border" >
                <input type="radio" name="q7" value="2" <?php if (isset($_POST["q7"]) && $_POST["q7"] == "2"){echo "checked";} ?>><a class="answer_text">Je to náročné, občas nezaplatím včas.</a>
            </label>
            <label class="answer_option_4q border" >
                <input type="radio" name="q7" value="3" <?php if (isset($_POST["q7"]) && $_POST["q7"] == "3"){echo "checked";} ?>><a class="answer_text">Zatím to zvládám, ale musím počítat.</a>
            </label>
            <label class="answer_option_4q border" >
                <input type="radio" name="q7" value="4" <?php if (isset($_POST["q7"]) && $_POST["q7"] == "4"){echo "checked";} ?>><a class="answer_text">Náklady na bydlení zvládám platit v pohodě.</a>
            </label>
        </div>
    </div>
    <div class="quiz_3q blur_background border">
        <h2 class="question">
            <label for="q8_range_input">Kolik měsíčně splácíte?</label>
        </h2>
        <input type="range" min="0" max="30000" step="1000" value="<?php if (isset($_POST["q8_amount"])){echo $_POST["q8_amount"];} else{echo "0";} ?>" name="q8_amount" class="range_input" id="q8_range_input">
        <output class="number_output" id="q8_output" name="result" for="q8_range_input"><?php if (isset($_POST["q8_amount"])){echo $_POST["q8_amount"];} else{echo "0";} ?> Kč</output>
    </div>
    <div class="quiz_3q blur_background border">
        <h2 class="question">
            <label for="q9_range_input">Kolik si měsíčně vyděláte?</label>
        </h2>
        <input type="range" min="1" max="6" value="<?php if (isset($_POST["q9_amount"])){echo $_POST["q9_amount"];} else{echo "1";} ?>" name="q9_amount" class="range_input" id="q9_range_input">
        <output class="number_output" id="q9_output" name="result" for="q9_range_input"><?php if (isset($_POST["q9_amount"])){switch ($_POST["q9_amount"]){ case "1": echo "0 - 20 tisíc Kč"; break; case "2": echo "20 - 30 tisíc Kč"; break; case "3": echo "30 - 45 tisíc Kč"; break; case "4": echo "45 - 65 tisíc Kč"; break; case "5": echo "65 - 90 tisíc Kč"; break; case "6": echo "90 tisíc Kč a více"; break;}} else{echo "0 - 20 tisíc Kč";}?></output>
    </div>
    <div class="quiz_4q blur_background border">
        <h2 class="question">Jak řešíte nečekané výdaje nebo výpadek příjmu?</h2>
        <label class="answer_option_4q border">
            <input type="radio" name="q10" value="1" <?php if (isset($_POST["q10"]) && $_POST["q10"] == "1"){echo "checked";} elseif (!isset($_POST["q1"])){echo "checked";} ?>><a class="answer_text">Nebankovní půjčkou nebo mimo finanční instituce.</a>
        </label>
        <label class="answer_option_4q border">
            <input type="radio" name="q10" value="2" <?php if (isset($_POST["q10"]) && $_POST["q10"] == "2"){echo "checked";} ?>><a class="answer_text">Zrušením investic, stavebního nebo penzijního spoření.</a>
        </label>
        <label class="answer_option_4q border">
            <input type="radio" name="q10" value="3" <?php if (isset($_POST["q10"]) && $_POST["q10"] == "3"){echo "checked";} ?>><a class="answer_text">Půjčkou, kreditkou nebo kontokorentem.</a>
        </label>
        <label class="answer_option_4q border">
            <input type="radio" name="q10" value="4" <?php if (isset($_POST["q10"]) && $_POST["q10"] == "4"){echo "checked";} ?>><a class="answer_text">Z ušetřených peněz.</a>
        </label>
    </div>
    <div class="quiz_3q blur_background border">
        <h2 class="question">Spoléháte na štěstí, nebo myslíte i na záchrannou brzdu?</h2>
        <div class="quiz_answeres_fix_3q">
            <label class="answer_option_3q border">
                <input type="checkbox" name="q11_options[]" value="1" id="q11_opt1a" <?php if ($q11_options != null && in_array("1", $q11_options)){echo "checked";} ?>><a class="answer_text">Mám vlastní bydlení.</a>
            </label>
            <label class="answer_option_3q border">
                <input type="checkbox" name="q11_options[]" value="2" id="q11_opt2a" <?php if ($q11_options != null && in_array("2", $q11_options)){echo "checked";} ?>><a class="answer_text">Splácím půjčky.</a>
            </label>
            <label class="answer_option_3q border">
                <input type="checkbox" name="q11_options[]" value="3" id="q11_opt3a" <?php if ($q11_options != null && in_array("3", $q11_options)){echo "checked";} ?>><a class="answer_text">Jezdím do zahraničí.</a>
            </label>
        </div>
        <div class="quiz_answeres_fix_4q">
            <label class="answer_option_4q border">
                <input type="checkbox" name="q11[]" id="q11_opt1b" value="1" <?php if ($q11 != null && in_array("1", $q11)){echo "checked";} ?>><a class="answer_text">Mám pojištění nemovitosti.</a>
            </label>
            <label class="answer_option_4q border">
                <input type="checkbox" name="q11[]" id="q11_opt2b" value="2" <?php if ($q11 != null && in_array("2", $q11)){echo "checked";} ?>><a class="answer_text">Mám pojištění schopnosti splácet.</a>
            </label>
            <label class="answer_option_4q border">
                <input type="checkbox" name="q11[]" id="q11_opt3b" value="3" <?php if ($q11 != null && in_array("3", $q11)){echo "checked";} ?>><a class="answer_text">Mám nebo si sjednávám cestovní pojištění.</a>
            </label>
            <label class="answer_option_4q border">
                <input type="checkbox" name="q11[]" value="4" id="q11_opt4" <?php if ($q11 != null && in_array("4", $q11)){echo "checked";} ?>><a class="answer_text">Mám životní či úrazové pojištění.</a>
            </label>
            <label class="answer_option_4q border">
                <input type="checkbox" name="q11[]" value="5" id="q11_opt5" <?php if ($q11 != null && in_array("5", $q11)){echo "checked";} ?>><a class="answer_text">Mám pojištění odpovědnosti za škodu.</a>
            </label>
            <label class="answer_option_4q border">
                <input type="checkbox" name="q11[]" value="6" id="q11_opt6" <?php if ($q11 != null && in_array("6", $q11)){echo "checked";} elseif ($q11 == null){echo "checked";} ?>><a class="answer_text">Nemám nic takového.</a>
            </label>
        </div>
    </div>
    <div class="quiz_4q blur_background border">
        <h2 class="question">Myslíte také na svou budoucnost? Pokud ano, kolik si na ni měsíčně přispíváte?</h2>
        <label class="answer_option_4q border" >
            <input type="radio" name="q12" value="1" <?php if (isset($_POST["q12"]) && $_POST["q12"] == "1"){echo "checked";} elseif (!isset($_POST["q1"])){echo "checked";} ?>><a class="answer_text">Zatím nic.</a>
        </label>
        <label class="answer_option_4q border" >
            <input type="radio" name="q12" value="2" <?php if (isset($_POST["q12"]) && $_POST["q12"] == "2"){echo "checked";} ?>><a class="answer_text">Do 1 000 Kč.</a>
        </label>
        <label class="answer_option_4q border" >
            <input type="radio" name="q12" value="3" <?php if (isset($_POST["q12"]) && $_POST["q12"] == "3"){echo "checked";} ?>><a class="answer_text">Do 3 000 Kč.</a>
        </label>
        <label class="answer_option_4q border" >
            <input type="radio" name="q12" value="4" <?php if (isset($_POST["q12"]) && $_POST["q12"] == "4"){echo "checked";} ?>><a class="answer_text">Více než 3 000 Kč.</a>
        </label>
    </div>
    <div class="quiz_submit_button blur_background border">
        <button class="submit_answer">
            Odeslat odpovědi
        </button>
    </div>
</form>
<script src="../scripts/rangeInputValue.js"></script>
<script src="../scripts/disableCheckboxes.js"></script>
<script src="../scripts/disabeledJavaScript.js"></script>
<script src="../scripts/quizFormCheck.js"></script>
</body>
</html>