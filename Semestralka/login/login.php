<?php
/**
 * Start a new session or resume an existing one.
 */
if (session_id() === '') {
   session_start();
}
include ("../php/setCookieForAppearance.php");

$fail = false;
include("../php/library.php");

/**
 * Check if the @param string $username exist in the users.json file,
 * And if the hashed value of @param string $password is the same as the one in users.json, under the users username.
 * If so, log the user in, under the @param string $username
 * Otherwise, deny the login and fill the username back into the input field.
 */
if (isset($_POST["username"]) and isset($_POST["password"])) {
    $username = htmlspecialchars($_POST["username"]);
    $password = $_POST["password"];
    $user = getUser($username);
    if ($user == "No such user exists"){
        $fail = true;
    }
    elseif (password_verify($password, $user["password"])) {
        $_SESSION["user"] = $user["username"];
        header("Location: ../home/home.php");
    }
    else{
        $fail = true;
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="login.css">
    <?php
    include("../php/appearanceGlobal.php");
    ?>
    <link rel="icon" href="../img/tab_icon.png">
    <title>Financial (W/H)ealth</title>
</head>
<body>
<?php
include("../header.php");
?>
    <main>
        <div class="white_logo">
            <img src="../img/png_white_logo.png" alt="Financial (W/H)ealth Logo">
        </div>
        <div class="login blur_background border">
            <div class="headline pad align_center">
                <h2>Přihlášení</h2>
            </div>
            <form method="post">
                <div class="align_in_row pad">
                    <div class="name">
                        <h3><label for="username">Uživatelské jméno: </label></h3>
                    </div>
                    <div class="form">
                        <input class="input" type="text" name="username" id="username" value="<?php if (isset($username)){echo $username;} ?>" placeholder="Uživatelské jméno" required>
                    </div>
                </div>
                <div class="align_in_row pad">
                    <div class="name">
                        <h3><label for="password">Heslo: </label></h3>
                    </div>
                    <div class="form">
                        <input class="input" type="password" name="password" id="password" placeholder="Heslo" required>
                    </div>
                </div>
                <div class="submit align_in_row pad">
                    <button id="login_button">Přihlásit se</button>
                </div>
                <div class="message">
                    <?php
                    if ($fail === true){
                        echo ("<h4 id='message4'>Uživatelské jméno, nebo heslo je špatné.</h4>");
                    }
                    ?>
                </div>
            </form>
            <div class="registration align_center">
                <a>Nemáte ještě účet? </a>
                <div class="reg_link">
                    &nbsp; &nbsp; <a href="../signup/signup.php">Zaregistrujte se.</a>
                </div>
            </div>
        </div>
    </main>
</body>
</html>