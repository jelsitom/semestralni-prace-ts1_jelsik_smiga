// Listen for input in the password field
let usernameElement = document.getElementById("username")
let pwElement1 = document.getElementById("password1")
let pwElement2 = document.getElementById("password2")
let form2 = document.getElementById("signup_form")

function checkPasswordOnInput() {
    // Clear the timeout if it has already been set
    if (typeof timeout !== 'undefined'){
        clearTimeout(timeout)
    }
    // Set a new timeout that will execute the event listener after 500 milliseconds
    timeout = setTimeout(function () {
        // Get the password from the field
        let password = pwElement1.value

        // Send the password to the server using an AJAX request
        let xhr = new XMLHttpRequest()
        xhr.open("POST", "../php/passwordStrengthCheck.php", true)
        xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded")
        xhr.onload = function () {
            if (xhr.status === 200) {
                // Update the UI based on the response from the server
                let strength = xhr.responseText
                if (strength === "strong") {
                    document.getElementById("message1").classList.add("hidden")
                    pwElement1.classList.remove("wrong")

                } else {
                    document.getElementById("message1").classList.remove("hidden")
                    pwElement1.classList.add("wrong")
                }
            }
        }
        xhr.send("password=" + password);
    }, 500)
}

function checkUsernameOnInput(){
    if (typeof  timeout !== 'undefined'){
        clearTimeout(timeout)
    }
    timeout = setTimeout(function () {
        let username = usernameElement.value
        let xhr = new XMLHttpRequest()
        xhr.open("POST", "../php/usernameCheck.php", true)
        xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded")
        xhr.onload = function () {
            if (xhr.status === 200) {
                let availability = xhr.responseText
                console.log(availability)
                if (availability === "free") {
                    document.getElementById("message3").classList.add("hidden")
                    usernameElement.classList.remove("wrong")
                } else {
                    document.getElementById("message3").classList.remove("hidden")
                    usernameElement.classList.add("wrong")
                }
            }
        }
        xhr.send("username=" + username);
    }, 500)
}

function checkInputs() {
    // Clear the timeout if it has already been set
    if (typeof timeout !== 'undefined'){
        clearTimeout(timeout);
    }
    timeout = setTimeout(function () {
        // Get the values of the two inputs
        const input1 = document.getElementById('password1').value;
        const input2 = document.getElementById('password2').value;
        // Check if the values do not match
        if (input1 !== input2) {
            // Prevent the default action of the event (e.g. form submission)
            document.getElementById("message2").classList.remove("hidden")
            pwElement2.classList.add("wrong")
        }
        else{
            document.getElementById("message2").classList.add("hidden")
            pwElement2.classList.remove("wrong")
        }
    }, 500)
}

function checkPasswordOnSubmit(callback) {
    let password = pwElement1.value;
    let xhr = new XMLHttpRequest();
    xhr.open("POST", "../php/passwordStrengthCheck.php");
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhr.onload = function () {
        // Call the callback function with the response from the server
        callback(xhr.responseText);
    };
    xhr.send("password=" + password);
}

function checkInputsOnSubmit(){
    const input1 = document.getElementById('password1').value
    const input2 = document.getElementById('password2').value
    if (input1 !== input2){
        return "no match"
    }
    else{
        return "match"
    }
}

function checkUsernameOnSubmit(callback){
    let username = usernameElement.value;
    let xhr = new XMLHttpRequest();
    xhr.open("POST", "../php/usernameCheck.php");
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhr.onload = function () {
        callback(xhr.responseText);
    };
    xhr.send("username=" + username);
}

function preventFormSending(event) {
    event.preventDefault();

    checkPasswordOnSubmit(function (passwordResponse) {
        checkUsernameOnSubmit(function (usernameResponse) {
            const inputMatch = checkInputsOnSubmit();
            if (passwordResponse === "strong" && usernameResponse === "free" && inputMatch === "match") {
                form2.submit();
            }
        });
    });
}

// Add an event listener to the form's submit event
pwElement1.addEventListener('input', checkInputs)
pwElement2.addEventListener('input', checkInputs)
pwElement1.addEventListener("input", checkPasswordOnInput)
form2.addEventListener("submit", preventFormSending)
usernameElement.addEventListener('input', checkUsernameOnInput)