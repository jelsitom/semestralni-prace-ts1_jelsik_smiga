document.querySelector('form').addEventListener('submit',
    function (event) {
        event.preventDefault(); // prevent the form from being submitted

        // get the form data
        var q1 = document.querySelector('[name=q1]:checked').value;
        var q2 = document.querySelector('[name=q2]:checked').value;
        var q3 = document.querySelector('[name=q3]:checked').value;
        var q4 = document.querySelector('[name=q4]:checked').value;
        var q5 = Number(document.querySelector('[name=q5]').value);
        var q6 = document.querySelector('[name=q6]:checked').value;
        var q7 = document.querySelector('[name=q7]:checked').value;
        var q8_amount = Number(document.querySelector('[name=q8_amount]').value);
        var q9_amount = Number(document.querySelector('[name=q9_amount]').value);
        var q10 = document.querySelector('[name=q10]:checked').value;
        var q12 = document.querySelector('[name=q12]:checked').value;
        var q11Elements = document.getElementsByName("q11[]");
        var q11 = [];
        for (var a = 0; a < q11Elements.length; a++) {
            if (q11Elements[a].checked) {
                q11.push(q11Elements[a].value);
            }
        }
        var q11OptionsElements = document.getElementsByName("q11_options[]");
        var q11_options = [];
        for (var b = 0; b < q11OptionsElements.length; b++) {
            if (q11OptionsElements[b].checked) {
                q11_options.push(q11OptionsElements[b].value);
            }
        }

        // check the form data
        if (!(["1", "2", "3"].includes(q1)) ||
            !(["1", "2", "3"].includes(q2)) ||
            !(["1", "2", "3", "4"].includes(q3)) ||
            !(["1", "2", "3"].includes(q4)) ||
            !(q5 >= 1000 && q5 <= 30000) ||
            !(["1", "2", "3"].includes(q6)) ||
            !(["1", "2", "3", "4"].includes(q7)) ||
            !(q8_amount >= 0 && q8_amount <= 30000) ||
            !(q9_amount >= 1 && q9_amount <= 6) ||
            !(["1", "2", "3", "4"].includes(q10)) ||
            !(["1", "2", "3", "4"].includes(q12))) {
            return false;
        }

        // check the q11_options and q11 form data
        if (q11 !== [] || q11_options !== []) {
            for (var i = 0; i < 3; i++) {
                if (q11.includes(toString(i + 1)) && !q11_options.includes(toString(i + 1))) {
                    return false;
                }
            }
            if (q11.includes('6') && q11.length >= 2) {
                return false;
            }
        }

        if (Array.isArray(q11) && q11.length === 0) {
            return false;
        }

        // form data is valid, submit the form
        this.submit();
    });
