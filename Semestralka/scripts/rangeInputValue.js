function showRangeInputValue(range_id, output_id){
    range = document.getElementById(range_id)
    output = document.getElementById(output_id)
    if (range.value === "30000"){
        output.innerHTML = "30000+ Kč"
    }
    else {
        output.innerHTML = range.value + " Kč"
    }
}

function showIncomeInputValue(range_id, output_id){
    range = document.getElementById(range_id)
    output = document.getElementById(output_id)
    switch (range.value){
        case "1":
            output.innerHTML = "0 - 20 tisíc Kč"
            break
        case "2":
            output.innerHTML = "20 - 30 tisíc Kč"
            break
        case "3":
            output.innerHTML = "30 - 45 tisíc Kč"
            break
        case "4":
            output.innerHTML = "45 - 65 tisíc Kč"
            break
        case "5":
            output.innerHTML = "65 - 90 tisíc Kč"
            break
        case "6":
            output.innerHTML = "90 tisíc Kč a více"
            break
    }
}

let q5 = document.getElementById("q5_range_input")
q5.addEventListener("input", () => {
    showRangeInputValue("q5_range_input","q5_output")
})
let q8 = document.getElementById("q8_range_input")
q8.addEventListener("input", () => {
    showRangeInputValue("q8_range_input","q8_output")
})
let q9 = document.getElementById("q9_range_input")
q9.addEventListener("input", () => {
    showIncomeInputValue("q9_range_input","q9_output");
});