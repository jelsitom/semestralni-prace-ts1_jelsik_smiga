function handleChange(checkbox1_id, checkbox2_id) {
    let checkbox1 = document.getElementById(checkbox1_id)
    let checkbox2 = document.getElementById(checkbox2_id)
    let checkbox6 = document.getElementById("q11_opt6")
    if (checkbox1.checked && !checkbox6.checked) {
        checkbox2.disabled = false
    } else {
        checkbox2.checked = false
        checkbox2.disabled = true
    }
}

function noInsuraceDisable() {
    let checkbox6 = document.getElementById("q11_opt6")
    let checkbox1b = document.getElementById("q11_opt1b")
    let checkbox2b = document.getElementById("q11_opt2b")
    let checkbox3b = document.getElementById("q11_opt3b")
    let checkbox4 = document.getElementById("q11_opt4")
    let checkbox5 = document.getElementById("q11_opt5")
    let checkbox1a = document.getElementById("q11_opt1a")
    let checkbox2a = document.getElementById("q11_opt2a")
    let checkbox3a = document.getElementById("q11_opt3a")
    if (checkbox6.checked) {
        checkbox1b.checked = false
        checkbox1b.disabled = true
        checkbox2b.checked = false
        checkbox2b.disabled = true
        checkbox3b.checked = false
        checkbox3b.disabled = true
        checkbox4.checked = false
        checkbox4.disabled = true
        checkbox5.checked = false
        checkbox5.disabled = true
    } else {
        if (checkbox1a.checked) {
            checkbox1b.disabled = false
        }
        if (checkbox2a.checked) {
            checkbox2b.disabled = false
        }
        if (checkbox3a.checked) {
            checkbox3b.disabled = false
        }
        checkbox4.disabled = false
        checkbox5.disabled = false
    }
}

let q11_1 = document.getElementById("q11_opt1a")
let q11_2 = document.getElementById("q11_opt2a")
let q11_3 = document.getElementById("q11_opt3a")
let q11_opt6 = document.getElementById("q11_opt6")
q11_1.addEventListener("input", () => {
    handleChange("q11_opt1a","q11_opt1b")
})
q11_2.addEventListener("input", () => {
    handleChange("q11_opt2a","q11_opt2b")
})
q11_3.addEventListener("input", () => {
    handleChange("q11_opt3a","q11_opt3b")
})
q11_opt6.addEventListener("input",noInsuraceDisable)