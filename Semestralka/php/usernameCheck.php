<?php
/**
 * Start a new session or resume an existing one.
 */
if (session_id() === '') {
    session_start();
}

/**
 * Include the library file
 */
include ("library.php");

/**
 * Get the @param string $username from the request
 */
$username = $_POST['username'];

/**
 * Check the availability of the @param string $username by calling the function from the library file
 */
$availability = checkUsername($username);

/**
 * Return the @param string $strength
 */
echo $availability;

?>