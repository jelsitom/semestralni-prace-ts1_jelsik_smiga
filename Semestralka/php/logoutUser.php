<?php
/**
 * Start a new session or resume an existing one.
 */
if (session_id() === '') {
    session_start();
}

/**
 * Unset the "user" variable from the session.
 */
unset($_SESSION["user"]);

/**
 * Redirect the user to the home page.
 */
header("Location: ../home/home.php");

?>