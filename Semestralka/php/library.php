<?php

/**
 * Reads the contents of the "../data/users.json" file, decodes the JSON data,
 * and returns it as an array. If the file does not exist, it returns an empty array.
 *
 * @return array The array of users.
 */
function getAllUsers(){
    if(!file_exists("../data/users.json")){return [];}
    $read_data = file_get_contents("../data/users.json");
    return json_decode($read_data, true);
}

/**
 * Encodes the provided data as JSON and writes it to the "../data/users.json" file.
 *
 * @param array $data The data to be written to the file.
 */
function writeUsers($data){
    $json = json_encode($data, JSON_PRETTY_PRINT);
    file_put_contents("../data/users.json", $json);
}

/**
 * Adds a new user to the list of users stored in the "../data/users.json" file.
 *
 * @param string $name The name of the user.
 * @param string $surname The surname of the user.
 * @param string $username The username of the user.
 * @param string $password The password of the user, hashed using the `password_hash` function.
 * @param string $id The ID of the user.
 */
function addUser($name, $surname, $username, $password1, $id) {
    $users = getAllUsers();
    $newUser = array(
        "name" => $name,
        "surname" => $surname,
        "username" => $username,
        "password" => password_hash($password1, PASSWORD_DEFAULT),
        "id" => $id
    );
    $users[] = $newUser;
    writeUsers($users);
}

/**
 * Checks if the provided string consists only of letters.
 *
 * @param string $name The string to be checked.
 * @return bool True if the string consists only of letters, False otherwise.
 */
function checkName($name){
    $pattern = '/^[a-zA-ZČčĎďĚěŇňŘřŠšŤťŮůÚúÝýÍíÁáÉéÓóŽž]+$/';
    if ($name === ""){
        return true;
    }
    elseif (preg_match($pattern, $name)) {
        return true;
    }
    else{
        return false;
    }
}

/**
 * Checks the strength of the provided password.
 *
 * @param string $password The password to be checked.
 * @return string 'strong' if the password is strong, 'weak' otherwise.
 */
function checkPasswordStrength($password) {
    // Check the length of the password
    if (strlen($password) <= 8) {
        return "weak";
    }

    // Check for the presence of uppercase letters, lowercase letters, numbers, and special characters
    $hasUppercase = preg_match('/[A-Z]/', $password);
    $hasLowercase = preg_match('/[a-z]/', $password);
    $hasNumbers = preg_match('/[0-9]/', $password);
    $hasSpecialCharacters = preg_match('/[^a-zA-Z0-9]/', $password);
    if (!$hasUppercase || !$hasLowercase || !$hasNumbers || !$hasSpecialCharacters) {
        return "weak";
    }
    // If the password passes all checks, it is strong
    return "strong";
}

/**
 * Searches for a user with the provided username in the list of users stored in the "../data/users.json" file
 * and returns the user data if found. If no such user exists, it returns the string "No such user exists".
 *
 * @param string $username The username of the user to be searched for.
 * @return mixed The user data if found, "No such user exists" otherwise.
 */
function getUser($username){
    $users = getAllUsers();
    foreach($users as $user){
        if ($user["username"] == $username){
            return $user;
        }
    }
    return "No such user exists";
}

/**
 * Checks if the provided username is already in use by another user.
 *
 * @param string $username The username to be checked.
 * @return string 'taken' if the username is already in use, 'free' otherwise.
 */
function checkUsername($username){
    $users = getAllUsers();
    foreach ($users as $user){
        if ($user["username"] == $username){
            return "taken";
        }
    }
    return "free";
}

/**
 * Reads the contents of the "../data/results.json" file, decodes the JSON data,
 * and returns it as an array. If the file does not exist, it returns an empty array.
 *
 * @return array The array of data.
 */
function getData(){
    if(!file_exists("../data/results.json")){return [];}
    $read_data = file_get_contents("../data/results.json");
    return json_decode($read_data, true);
}

/**
 * Encodes the provided data as JSON and writes it to the "../data/results.json" file.
 *
 * @param array $data The data to be written to the file.
 */
function writeData($data){
    $json = json_encode($data, JSON_PRETTY_PRINT);
    file_put_contents("../data/results.json", $json);
}

/**
 * Adds a new set of data to the list of results stored in the "../data/results.json" file.
 * The data consists of answers to various questions (q1, q2, etc.), as well as an id and a user.
 * The current timestamp is also included in the data.
 *
 * @param string $q1 The answer to question 1.
 * @param string $q2 The answer to question 2.
 * @param string $q3 The answer to question 3.
 * @param string $q4 The answer to question 4.
 * @param string $q5 The answer to question 5.
 * @param string $q6 The answer to question 6.
 * @param string $q7 The answer to question 7.
 * @param string $q8_amount The answer to question 8.
 * @param string $q9_amount The answer to question 9.
 * @param string $q10 The answer to question 10.
 * @param string $q11_options The answer to question 11 options.
 * @param string $q11 The answer to question 11.
 * @param string $q12 The answer to question 12.
 */
function addData($q1, $q2, $q3, $q4, $q5, $q6, $q7, $q8_amount, $q9_amount, $q10, $q11_options, $q11, $q12){
    $data = getData();
    $newData = array(
        "q1" => $q1,
        "q2" => $q2,
        "q3" => $q3,
        "q4" => $q4,
        "q5" => $q5,
        "q6" => $q6,
        "q7" => $q7,
        "q8_amount" => $q8_amount,
        "q9_amount" => $q9_amount,
        "q10" => $q10,
        "q11_options" => $q11_options,
        "q11" => $q11,
        "q12" => $q12,
        "time" => time(),
        "user" => $_SESSION["user"]
    );
    $data[] = $newData;
    writeData($data);
}

/**
 * Checks if the provided data for the quiz form is valid. Specifically, it checks that the selected options for
 * question 11 are consistent with the value of question 11. If the data is invalid, it returns false. Otherwise,
 * it returns true.
 *
 * @param array $q11 The value of question 11
 * @param array $q11_options The selected options for question 11
 *
 * @return bool whether the form data is valid or not.
 */
function checkQuizFormData($q11, $q11_options){
    if ($q11 === null || $q11_options === null){
        return false;
    }
    for ($i=1; $i <= 3; $i++){
        if (in_array($i, $q11) and !in_array($i, $q11_options)){
            return false;
        }
        if (in_array("6", $q11) and count($q11) >= 2){
            return false;
        }
    }
    return true;
}

/**
 * Finds the latest quiz submission for the user saved in $_SESSION["user"] and returns it as an array.
 * If no submissions have been made by the user, it returns null.
 *
 * @return array|null The latest quiz submission for the user or null if no submissions have been made.
 */
function getNewestData(){
    $data = getData();
    if ($data == []){
        return null;
    }
    // Find the data for the user saved in $_SESSION["user"]
    $user_data = [];
    foreach ($data as $item) {
        if ($item["user"] == $_SESSION["user"]) {
            $user_data[] = $item;
        }
    }
    // Find the data with the largest time value
    $last_input = null;
    foreach ($user_data as $item) {
        if ($last_input == null || $item['time'] > $last_input['time']) {
            $last_input = $item;
        }
    }
    return $last_input;
}

