<?php

/**
 * Start a new session or resume an existing one
 */
if (session_id() === '') {
    session_start();
}

/**
 * Set the default value for the @param string $mode variable
 */
$mode = "normal";

/**
 * Check if the "mode" value is being passed in the URL
 */
if (isset($_GET["mode"])) {
    /**
     * If it is, set the @param string $mode variable to the value passed in the URL
     */
    $mode = $_GET["mode"];
}

/**
 * Check if the "mode" value is being passed in a cookie
 */
elseif (isset($_COOKIE["mode"])) {
    /**
     * If it is, set the @param string $mode variable to the value passed in the cookie
     */
    $mode = $_COOKIE["mode"];
}

/**
 * Echo out a link to the appropriate stylesheet based on the mode variable
 */
echo ('<link rel="stylesheet" href="../'.$mode.'.css">');

?>