<?php

/**
 * Start a new session or resume an existing one
 */
if (session_id() === '') {
    session_start();
}

/**
 * Set the default value for the @param string $mode variable
 */
$mode = "results1";

/**
 * Check if the "mode" value is being passed in the URL
 */
if (isset($_GET["mode"])) {
    /**
     * Check if the value is "colorless"
     */
    if ($_GET["mode"] == "colorless") {
        /**
         * If it is, set the @param string $mode variable to "results2"
         */
        $mode = "results2";
    } else {
        /**
         * If not, set the @param string $mode variable to "results1"
         */
        $mode = "results1";
    }
}

/**
 * Check if the "mode" value is being passed in a cookie
 */
elseif (isset($_COOKIE["mode"])) {
    /**
     * Check if the value is "colorless"
     */
    if ($_COOKIE["mode"] == "colorless") {
        /**
         * If it is, set the @param string $mode variable to "results2"
         */
        $mode = "results2";
    } else {
        /**
         * If not, set the @param string $mode variable to "results1"
         */
        $mode = "results1";
    }
}

/**
 * Echo out a link to the appropriate stylesheet based on the mode variable
 */
echo ('<link rel="stylesheet" href="'.$mode.'.css">');

?>