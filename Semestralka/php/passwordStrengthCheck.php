<?php
/**
 * Start a new session or resume an existing one.
 */
if (session_id() === '') {
    session_start();
}

/**
 * Include the library file
 */
include ("library.php");

/**
 * Get the @param string $password from the request
 */
$password = $_POST['password'];

/**
 * Check the strength of the @param string $password by calling the function from the library file
 */
$strength = checkPasswordStrength($password);

/**
 * Return the @param string $strength
 */
echo $strength;

?>