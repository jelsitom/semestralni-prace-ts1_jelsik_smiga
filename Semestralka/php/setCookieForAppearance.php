<?php
/**
 * Start a new session or resume an existing one.
 */
if (session_id() === '') {
    session_start();
}

/**
 * Set the @param int $lifetime of the cookie to 1 year
 */
$lifetime = strtotime(" +1 year");

/**
 * Set the default value for the @param string $mode variable
 */
$mode = "normal";

/**
 * Check if the "mode" value is being passed in the URL
 */
if (isset($_GET["mode"])) {
    /**
     * If it is, set the @param string $mode variable to the value passed in the URL
     */
    $mode = $_GET["mode"];

    /**
     * Save the "mode" value in a cookie
     */
    setcookie("mode", $mode, $lifetime, "/~smigapav/Semestralka/");
}

/**
 * Check if the "mode" value is being passed in a cookie
 */
elseif (isset($_COOKIE["mode"])) {
    /**
     * If it is, set the @param string $mode variable to the value passed in the cookie
     */
    $mode = $_COOKIE["mode"];
}

?>