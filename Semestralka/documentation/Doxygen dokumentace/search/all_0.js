var searchData=
[
  ['_24availability_0',['$availability',['../username_check_8php.html#a9c96342490e441c688201e273573f682',1,'usernameCheck.php']]],
  ['_24current_5fpage_1',['$current_page',['../results_8php.html#a8ed60f12a4d49a92cadd596fd881b904',1,'$current_page():&#160;results.php'],['../testing_8php.html#a8ed60f12a4d49a92cadd596fd881b904',1,'$current_page():&#160;testing.php']]],
  ['_24data_2',['$data',['../results_8php.html#a6efc15b5a2314dd4b5aaa556a375c6d6',1,'results.php']]],
  ['_24fail_3',['$fail',['../login_8php.html#a3a806d49001007bba46305cad5e2f466',1,'login.php']]],
  ['_24lifetime_4',['$lifetime',['../set_cookie_for_appearance_8php.html#a5e3b3a4f16a9e80d6ab0408b8df110dd',1,'setCookieForAppearance.php']]],
  ['_24mode_5',['$mode',['../appearance_global_8php.html#a74f868c16a89d492ce00a5f10e129b31',1,'$mode():&#160;appearanceGlobal.php'],['../appearance_home_8php.html#a74f868c16a89d492ce00a5f10e129b31',1,'$mode():&#160;appearanceHome.php'],['../appearance_quiz_8php.html#a74f868c16a89d492ce00a5f10e129b31',1,'$mode():&#160;appearanceQuiz.php'],['../appearance_results_8php.html#a74f868c16a89d492ce00a5f10e129b31',1,'$mode():&#160;appearanceResults.php'],['../set_cookie_for_appearance_8php.html#a3aaf40baac36e278c7d7c9139df1750c',1,'$mode():&#160;setCookieForAppearance.php']]],
  ['_24pages_6',['$pages',['../testing_8php.html#a263621399c53f2952c2329ee13ad8e4e',1,'testing.php']]],
  ['_24paragraphs_7',['$paragraphs',['../testing_8php.html#a9f4db732df64cc19139952aff9867004',1,'testing.php']]],
  ['_24password_8',['$password',['../password_strength_check_8php.html#a607686ef9f99ea7c42f4f3dd3dbb2b0d',1,'passwordStrengthCheck.php']]],
  ['_24per_5fpage_9',['$per_page',['../testing_8php.html#abece0f3099457a037f8f339811dc6e20',1,'testing.php']]],
  ['_24start_10',['$start',['../testing_8php.html#a50a00e7de77365e00b117e73aa82fb9b',1,'testing.php']]],
  ['_24strength_11',['$strength',['../password_strength_check_8php.html#ad03307067685557c77a781409e559f4b',1,'passwordStrengthCheck.php']]],
  ['_24total_5fpages_12',['$total_pages',['../results_8php.html#a75dd972125f47d333492e70523d1aa6a',1,'$total_pages():&#160;results.php'],['../testing_8php.html#acd30ad6e3dc17a63b8018b01f8621435',1,'$total_pages():&#160;testing.php']]],
  ['_24username_13',['$username',['../username_check_8php.html#a0eb82aa5f81cf845de4b36cd653c42cf',1,'usernameCheck.php']]]
];
