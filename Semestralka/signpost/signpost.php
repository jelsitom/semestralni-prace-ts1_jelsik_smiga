<?php
/**
 * Start a new session or resume an existing one.
 */
if (session_id() === '') {
   session_start();
}

include ("../php/setCookieForAppearance.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php
    include("../php/appearanceGlobal.php");
    ?>
    <link rel="icon" href="../img/tab_icon.png">
    <link rel="stylesheet" href="signpost.css">
    <title>Financial (W/H)ealth</title>
</head>
<body>
<main>
    <a class="blur_background panel" href="../home/home.php"><h1>Úvod</h1></a>
    <a class="blur_background panel" href="../login/login.php"><h1>Přihlásit se</h1></a>
    <a class="blur_background panel" href="../signup/signup.php"><h1>Registrovat se</h1></a>
</main>
</body>
</html>